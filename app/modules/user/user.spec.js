const userService = require('./user.service');

describe('Test User Service methods', () => {
    it('Test isSouthOrNorth -> is North', async () => {
        const coordinates = {
            latitude: 0,
            longitude: 0
        };
        console.log(coordinates, '<<<<< Test isSouthOrNorth -> is North');
        const result = await userService.isSouthOrNorth(coordinates.latitude, coordinates.longitude);
        expect(result).toBe('N');
    });

    it('Test isSouthOrNorth -> is South', async () => {
        const coordinates = {
            latitude: -1,
            longitude: 180
        };
        console.log(coordinates, '<<<<< Test isSouthOrNorth -> is South');
        const result = await userService.isSouthOrNorth(coordinates.latitude, coordinates.longitude);
        expect(result).toBe('S');
    });
});
