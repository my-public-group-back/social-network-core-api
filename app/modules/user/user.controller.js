'use strict';

const moduleConfig = require('./user.config');
const userService = require('./user.service');
const User = require('./user.entity');

// Auth Servive is not implemented for this project because is test project.
class UserController {
    async create(req, res)  {
        console.log('POST /users');
        console.log(req.body, '<<<<<<<< REQUEST BODY');
        const wrongBody = await userService.checkBodyData(req.body); // Check required User fields.
        if (wrongBody.result) {
            await res.status(400).send( { error: true, data: 'invalid request body data: ' + wrongBody.field } );
            return;
        }
        const userData = new User(req.body); // Instance User Model
        try {
            // Check if user is isSouthOrNorth
            const isSouthOrNorth = await userService.isSouthOrNorth(userData.latitude, userData.longitude);
            console.log(isSouthOrNorth, '<<<<<<<<<< IS SOUTH (S) OR NORTH (N)');
            if(isSouthOrNorth === 'N') { // Is Northern -> Create User
                const user = await userService.create(userData);
                await res.status(201).send({ error: false, data: user });
            } else { // Is Southern -> Send data to external API
                await userService.processSouthern(userData);
                await res.status(201).send({ error: false, data: 'southern user created' });
            }
        }
        catch(error){
            await res.status(500).send({ error: true, data: {} });
        }
    };

    async findAll(req, res)  {
        console.log('GET /users');
        try {
            const users = await userService.findAll();
            await res.status(200).send({ error: false, data: users });
        }
        catch(error){
            await res.status(500).send({ error: true, data: {} });
        }
    };

    async findOne(req, res) {
        const idUser = req.params[moduleConfig.api.param];
        console.log('GET /users/userId -> ' + idUser);
        try {
            const user = await userService.findOne(idUser);
            if (user) {
                await res.status(200).send({error: false, data: user});
            } else {
                await res.status(404).send({error: true, data: 'user not found'});
            }
        }
        catch(error){
            await res.status(500).send({ error: true, data: {} });
        }
    };

    async update(req, res) {
        const idUser = req.params[moduleConfig.api.param];
        console.log('PUT /users/userId -> ' + idUser);
        console.log(req.body, '<<<<<<<< REQUEST BODY');
        const wrongBody = await userService.checkBodyData(req.body);
        if (wrongBody.result) {
            await res.status(400).send( { error: true, data: 'invalid request body data: ' + wrongBody.field  } );
            return;
        }
        const userData = new User(req.body); // Instance User Model
        try {
            // Check if new data of User is isSouthOrNorth
            const isSouthOrNorth = await userService.isSouthOrNorth(userData.latitude, userData.longitude);
            console.log(isSouthOrNorth, '<<<<<<<<<< IS SOUTH (S) OR NORTH (N)');
            if(isSouthOrNorth === 'N') { // Is Northern -> Update User
                const user = await userService.update(userData, idUser);
                if (user) {
                    await res.status(200).send({error: false, data: user});
                } else {
                    await res.status(404).send({error: true, data: 'user not found'});
                }
            } else { // Is Southern -> Send data to external API
                await userService.processSouthern(userData);
                await res.status(200).send({ error: false, data: 'southern user updated' });
            }
        }
        catch(error){
            await res.status(500).send({ error: true, data: {} });
        }
    };

    async delete(req, res) {
        const idUser = req.params[moduleConfig.api.param];
        console.log('DELETE /users/userId -> ' + idUser);
        try {
            const result = await userService.delete(req.params[moduleConfig.api.param]);
            if (result.affectedRows > 0) {
                await res.status(200).send({ error: false, data: {} });
            } else {
                await res.status(404).send({error: true, data: 'user not found'});
            }
        }
        catch(error){
            await res.status(500).send({ error: true, data: {} });
        }
    };
}
module.exports = new UserController();
