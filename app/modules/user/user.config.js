const moduleConfig = {
    api: {
        route: 'users',
        param: 'userId',
    },
};

module.exports = moduleConfig;
