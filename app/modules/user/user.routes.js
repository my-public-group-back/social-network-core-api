'use strict';

const moduleConfig = require('./user.config');
const userController = require('./user.controller');

module.exports = async (app) => {
    // user Routes
    app.route(`/${moduleConfig.api.route}`)
        .get(await userController.findAll)
        .post(await userController.create);

    app.route(`/${moduleConfig.api.route}/:${moduleConfig.api.param}`)
        .get(await userController.findOne)
        .put(await userController.update)
        .delete(await userController.delete);
};
