'use strict';

const moduleConfig = require('./user.config');
const AbstractMysqlService = require('../../shared/services/abstract.mysql.service');

class UserService extends AbstractMysqlService{
    constructor(modelName) {
        super(modelName);
    }

    async isSouthOrNorth(latitude, longitude) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (latitude >= 0 && latitude <= 90
                    && longitude >= -180 && longitude <= 180) {
                    resolve('N');
                } else if (latitude < 0 && latitude >= -90
                    && longitude >= -180 && longitude <= 180){
                    resolve('S');
                } else {
                    reject(new Error('Bad values'));
                }
            }, 700);
        });
    };

    async processSouthern(data) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, 1000);
        });
    };

    async checkBodyData(userData) {
        if(!userData.username) {
            return {
                result: true,
                field: 'username'
            };
        }
        if(!userData.email) {
            return {
                result: true,
                field: 'email'
            };
        }
        if(!userData.password) {
            return {
                result: true,
                field: 'password'
            };
        }
        if(!userData.latitude) {
            return {
                result: true,
                field: 'latitude'
            };
        }
        if(!userData.longitude) {
            return {
                result: true,
                field: 'longitude'
            };
        }
        if(!userData.language) {
            return {
                result: true,
                field: 'language'
            };
        }
        return {
            result: false,
            field: ''
        };
    }
}
module.exports = new UserService(moduleConfig.api.route);
