const bcrypt = require('bcrypt');
const saltRounds = 10;

class User {
    constructor(user) {
        this.username = user.username;
        this.email = user.email;
        this.password = bcrypt.hashSync(user.password, saltRounds);
        this.latitude = user.latitude;
        this.longitude = user.longitude;
        this.language = user.language;
    }
}

module.exports = User;
