'use strict';

const userRoutes = require('./modules/user/user.routes');

const appRoutes = async (app) => {
    // app Routes
    app.route('/')
        .get(async (req, res) => {
            console.log('GET /');
            await res.send({ error: false, data: 'Hello World' });
        });
    // app userRoutes
    await userRoutes(app);

    app.route('*').all(async (req, res) => {
        console.log('Invalid Route');
        await res.send({ error: true, data: 'Invalid Route'});
    });
};

module.exports = appRoutes;
