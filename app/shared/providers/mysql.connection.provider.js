const mysql = require('mysql');
const mysqlConnectionConfig = require('../../config/mysql.db.config');

let mysqlConnection = mysql.createPool(mysqlConnectionConfig);

const handleDisconnect = (_connDB) => {
    _connDB.on('error', function(error){
        if(!error.fatal)  return;
        if(error.code !== 'PROTOCOL_CONNECTION_LOST')  throw error;

        console.log("re-connecting with mysql server!");
        console.log(mysqlConnectionConfig, '<<<<<< mysqlConnectionConfig');

        mysqlConnection = mysql.createConnection(mysqlConnectionConfig);
        console.log(mysqlConnection, '<<<<<< mysqlConnection');

        mysqlConnection.connect();
        handleDisconnect(mysqlConnection);
    });
};

handleDisconnect(mysqlConnection);

module.exports = mysqlConnection;
