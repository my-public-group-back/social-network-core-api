'use strict';

const mysqlConnection = require('../providers/mysql.connection.provider');
const first = require('lodash/first');

class AbstractMysqlService {
    constructor(modelName) {
        this.modelName = modelName;
        this.insertQuery = 'INSERT INTO '+ this.modelName +' set ? ';
        this.findAllQuery = 'SELECT * FROM '+ this.modelName;
        this.findOneQuery = 'SELECT * FROM '+ this.modelName +' where id = ?';
        this.updateQuery = 'UPDATE '+ this.modelName +' SET ? WHERE id = ?';
        this.deleteQuery = 'DELETE FROM '+ this.modelName +' where id = ?';
    }

    async create(model) {
        return new Promise(async (resolve, reject) => {
            await mysqlConnection.query(this.insertQuery, model, async (error, result) => {
                if (error) {
                    reject(new Error() );
                }
                resolve(await this.findOne(result.insertId));
            });
        });
    };

    async findAll() {
        return new Promise(async (resolve, reject) => {
            await mysqlConnection.query(this.findAllQuery, (error, result) => {
                if (error) {
                    reject(new Error() );
                }
                resolve(result);
            });
        });
    };

    async findOne(idModel) {
        return new Promise(async (resolve, reject) => {
            await mysqlConnection.query(this.findOneQuery, idModel, (error, result) => {
                if (error) {
                    reject(new Error() );
                }
                resolve(first(result));
            });
        });
    };

    async update(model, idModel) {
        return new Promise(async (resolve, reject) => {
            await mysqlConnection.query(this.updateQuery, [ model, idModel ], async (error) => {
                if (error) {
                    reject(new Error() );
                }
                resolve(await this.findOne(idModel));
            });
        });
    };

    async delete(idModel) {
        return new Promise(async (resolve, reject) => {
            await mysqlConnection.query(this.deleteQuery, idModel, (error, result) => {
                if (error) {
                    reject(new Error() );
                }
                resolve(result);
            });
        });
    };
}

module.exports = AbstractMysqlService;

