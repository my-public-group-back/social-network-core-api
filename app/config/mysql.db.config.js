const mysqlConnectionConfig = {
    localhost: {
        host     : 'localhost',
        user     : 'root',
        password : 'root',
        database : 'mysql_social_network_core_api'
    },
    production: {
        host: 'eu-cdbr-west-02.cleardb.net',
        user: 'b600c43dbf0679',
        password: 'bf30a3ca',
        database: 'heroku_b43ef8882e443e5',
        reconnect: true
    }
};

module.exports = process.env.NODE_ENV === 'production' ? mysqlConnectionConfig.production : mysqlConnectionConfig.localhost;
