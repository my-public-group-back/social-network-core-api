let chai = require('chai');
const expect = require('chai').expect;
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

let shortid = require('shortid');

describe('[API] User Endpoints (e2e)', () => {
    const should = chai.should();
    const fqdn = shortid.generate().replace(/[_-]/g, '');
    const url= 'http://localhost:3000';
    const route = '/users/';
    let userNorthernCreated;
    let userNorthernUpdated;

    const userNorthernCreateData = {
        username: shortid.generate(),
        email: `${fqdn}@${fqdn}.com`,
        password: shortid.generate(),
        latitude: 80,
        longitude: 1,
        language: shortid.generate()
    };

    const userNorthernCreateWrongData = {
        username: shortid.generate(),
    };

    const userNorthernUpdateData = {
        username: `${shortid.generate()}_CHANGED`,
        email: `${fqdn}@${fqdn}_CHANGED.com`,
        password: `${shortid.generate()}_CHANGED`,
        latitude: 90,
        longitude: 2,
        language: `${shortid.generate()}_CHANGED`,
    };

    const userSouthernCreateData = {
        username: shortid.generate(),
        email: `${fqdn}@${fqdn}.com`,
        password: shortid.generate(),
        latitude: -80,
        longitude: 1,
        language: shortid.generate()
    };

    const userSouthernUpdateData = {
        username: `${shortid.generate()}_CHANGED`,
        email: `${fqdn}@${fqdn}_CHANGED.com`,
        password: `${shortid.generate()}_CHANGED`,
        latitude: -80,
        longitude: 2,
        language: `${shortid.generate()}_CHANGED`,
    };

    it('Create a new northern user', async (done) => {
        chai.request(url)
            .post(route)
            .send(userNorthernCreateData)
            .set('Content-Type', 'application/json')
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                userNorthernCreated = response.body.data;
                expect(response).to.have.status(201);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                response.body.data.should.have.property('id', userNorthernCreated.id);
                response.body.data.should.have.property('username', userNorthernCreateData.username);
                response.body.data.should.have.property('email', userNorthernCreateData.email);
                response.body.data.should.have.property('latitude', userNorthernCreateData.latitude);
                response.body.data.should.have.property('longitude', userNorthernCreateData.longitude);
                response.body.data.should.have.property('language', userNorthernCreateData.language);
                return done();
            });
    });

    it('Create a new southern user', async (done) => {
        chai.request(url)
            .post(route)
            .send(userSouthernCreateData)
            .set('Content-Type', 'application/json')
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(201);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                response.body.should.have.property('data', 'southern user created');
                return done();
            });
    });

    it('Create a new user with wrong data', async (done) => {
        chai.request(url)
            .post(route)
            .send(userNorthernCreateWrongData)
            .set('Content-Type', 'application/json')
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(400);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', true);
                return done();
            });
    });

    it('Get one northern user', async (done) => {
        chai.request(url)
            .get(route + userNorthernCreated.id)
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(200);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                response.body.data.should.have.property('id', userNorthernCreated.id);
                response.body.data.should.have.property('username', userNorthernCreated.username);
                response.body.data.should.have.property('email', userNorthernCreated.email);
                response.body.data.should.have.property('latitude', userNorthernCreated.latitude);
                response.body.data.should.have.property('longitude', userNorthernCreated.longitude);
                response.body.data.should.have.property('language', userNorthernCreated.language);
                return done();
            });
    });

    it('Get not exists northern user', async (done) => {
        chai.request(url)
            .get(route + Math.floor(Math.random() * 1000))
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(404);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', true);
                return done();
            });
    });

    it('Get all northern users', async (done) => {
        chai.request(url)
            .get(route)
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(200);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                response.body.data.should.be.instanceof(Array);
                response.body.data.should.have.length.above(1);
                return done();
            });
    });

    it('Update one user with northern coordinates', async (done) => {
        chai.request(url)
            .put(route + userNorthernCreated.id)
            .send(userNorthernUpdateData)
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                userNorthernUpdated = response.body.data;
                expect(response).to.have.status(200);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                response.body.data.should.have.property('id', userNorthernCreated.id);
                response.body.data.should.have.property('username', userNorthernUpdateData.username);
                response.body.data.should.have.property('email', userNorthernUpdateData.email);
                response.body.data.should.have.property('latitude', userNorthernUpdateData.latitude);
                response.body.data.should.have.property('longitude', userNorthernUpdateData.longitude);
                response.body.data.should.have.property('language', userNorthernUpdateData.language);
                return done();
            });
    });

    it('Update one user with southern coordinates', async (done) => {
        chai.request(url)
            .put(route + userNorthernCreated.id)
            .send(userSouthernUpdateData)
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                userNorthernUpdated = response.body.data;
                expect(response).to.have.status(200);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                response.body.should.have.property('data', 'southern user updated');

                return done();
            });
    });

    it('Update not exists user', async (done) => {
        chai.request(url)
            .put(route + Math.floor(Math.random() * 1000))
            .send(userNorthernUpdateData)
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                userNorthernUpdated = response.body.data;
                expect(response).to.have.status(404);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', true);
                return done();
            });
    });

    it('Delete one northern user', async (done) => {
        chai.request(url)
            .delete(route + userNorthernCreated.id)
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(200);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                return done();
            });
    });

    it('Delete not exists northern user', async (done) => {
        chai.request(url)
            .delete(route + Math.floor(Math.random() * 1000))
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(404);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', true);
                return done();
            });
    });
});
