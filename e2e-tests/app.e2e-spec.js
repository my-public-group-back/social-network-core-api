let chai = require('chai');
const expect = require('chai').expect;
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

let shortid = require('shortid');

describe('[API] User Endpoints (e2e)', () => {
    const should = chai.should();
    const url= 'http://localhost:3000';
    const route = '/';


    it('Create a new northern user', async (done) => {
        chai.request(url)
            .get(route)
            .set('Content-Type', 'application/json')
            .end( async (error,response) => {
                if (error) {
                    return done(error);
                }
                expect(response).to.have.status(200);
                expect(response).to.have.header('Content-Type', /json/);
                response.body.should.have.property('error', false);
                response.body.should.have.property('data', 'Hello World');
                return done();
            });
    });
});
