## Description

Node and Express Social Network Core Api

[Social Media API-Core](https://gitlab.com/my-public-group-back/social-network-core-api) of Users

## Queries for Friendships table

- Get Friends Data of one User

```sql
Select * from users
where id in 
(select user_1 from friendships where user_2 = $id_user
union
select user_2 from friendships where user_1 = $id_user)
```

- Get number of Friends of one User

```sql
Select count(*) from users
where id in 
(select user_1 from friendships where user_2 = $id_user
union
select user_2 from friendships where user_1 = $id_user)
```

## Architecture
```bash
social-network-core-api
  ¬ app
    app.routes.js
    ¬¬ config
       mysql.db.config.js
    ¬¬ modules
      ¬¬¬ user
          user.config.js
          user.controller.js
          user.entity.js
          user.routes.js
          user.service.js
          user.spec.js
    ¬¬ shared
      ¬¬¬ providers
          mysql.connection.provider.js
      ¬¬¬ services
          abstract.mysql.service.js
  ¬ e2e-tests
    app.e2e-spec.js
    jest.e2e.json
    user.e2e-spec.js
  package-lock.json
  package.json
  README.md
  server.js
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start:dev

# production
$ npm run start
```
## Access the app

```bash
# Endpoints
-  Create User: POST https://social-network-core-api.herokuapp.com/users
   Body: {
         	"username": "prueba",
         	"email": "prueba@email.com",
         	"password": "passPrueba",
         	"latitude": 80,
         	"longitude": 1,
         	"language": "spanish"
         }
-  Get All Users: GET https://social-network-core-api.herokuapp.com/users

-  Get One User: GET https://social-network-core-api.herokuapp.com/users/:id

-  Update One User: PUT https://social-network-core-api.herokuapp.com/users/:id
    Body: {
         	"username": "prueba",
         	"email": "prueba@email.com",
         	"password": "passPrueba",
         	"latitude": 80,
         	"longitude": 1,
         	"language": "spanish"
          }
-  Delete One User: DELETE https://social-network-core-api.herokuapp.com/users/:id
```

## Test

```bash
# e2e tests
$ npm run test:e2e

# unit tests
$ npm run test
```

## Stay in touch

- Author - [Jose Lorenzo Moreno Moreno](https://www.linkedin.com/in/jolorenzom/)

## License

  This project is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).
