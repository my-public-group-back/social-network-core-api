const express = require('express');
app = express();

const bodyParser = require('body-parser');


const server = app.listen(process.env.PORT || 3000, () => {
    const host = server.address().address;
    const port = server.address().port;

    console.log('App listening at http://%s:%s', host, port);
    console.log('NODE_ENV: ' + process.env.NODE_ENV);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const appRoutes = require('./app/app.routes');
appRoutes(app);

module.exports = server;
